package br.com.ozcorp;

public class FuncionariosTeste {

	public static void main(String[] args) {
		
		Diretor diretor = new Diretor();
		diretor.Diretor();
		
		Secretario secretario = new Secretario();
		secretario.Secretario();
		
		Gerente gerente = new Gerente();
		gerente.Gerente();
		
		Engenheiro engenheiro = new Engenheiro();
		engenheiro.Engenheiro();
		
		Analista analista = new Analista();
		analista.Analista();
		
	}
}
