package br.com.ozcorp;

public class Secretario extends Funcionario {
	
	void Secretario() {

		Secretario flash = new Secretario();
		flash.setNome("Flash");
		flash.setRg("99.999.999-X");
		flash.setCpf("456.581.987-66");
		flash.setMatricula("1623545");
		flash.setEmail("allen.barry@liga.com");
		flash.setSenha("correLA");
		flash.setCargo("Secretário de Compras");
		flash.setSalario( 8000.00);
		flash.setTs("B+");
		flash.setSexo("Masculino");
		flash.setAcesso(1);

		System.out.println("NOME:               " + flash.getNome());
		System.out.println("RG:                 " + flash.getRg());
		System.out.println("CPF:                " + flash.getCpf());
		System.out.println("MATRICULA:          " + flash.getMatricula());
		System.out.println("EMAIL:              " + flash.getEmail());
		System.out.println("SENHA:              " + flash.getSenha());
		System.out.println("CARGO:              " + flash.getCargo());
		System.out.println("SALARIO:            " + flash.getSalario());
		System.out.println("TIPO SANGUINEO:     " + flash.getTs());
		System.out.println("SEXO:               " + flash.getSexo());
		System.out.println("NIVEL DE ACESSO:    " + flash.getAcesso());
		System.out.println("\n\n\n");
	}
}
