package br.com.ozcorp;

public class Diretor extends Funcionario {

	void Diretor() {

		Diretor batman = new Diretor();
		batman.setNome("Batman");
		batman.setRg("34.234.563-B");
		batman.setCpf("268.627.828-51");
		batman.setMatricula("1010569");
		batman.setEmail("knight.dark@liga.com");
		batman.setSenha("senpai666");
		batman.setCargo("Diretor Financeiro");
		batman.setSalario(10000.00);
		batman.setTs("A+");
		batman.setSexo("Masculino");
		batman.setAcesso(0);

		System.out.println("NOME:               " + batman.getNome());
		System.out.println("RG:                 " + batman.getRg());
		System.out.println("CPF:                " + batman.getCpf());
		System.out.println("MATRICULA:          " + batman.getMatricula());
		System.out.println("EMAIL:              " + batman.getEmail());
		System.out.println("SENHA:              " + batman.getSenha());
		System.out.println("CARGO:              " + batman.getCargo());
		System.out.println("SALARIO:            " + batman.getSalario());
		System.out.println("TIPO SANGUINEO:     " + batman.getTs());
		System.out.println("SEXO:               " + batman.getSexo());
		System.out.println("NIVEL DE ACESSO:    " + batman.getAcesso());
		System.out.println("\n\n\n");
	}
}
