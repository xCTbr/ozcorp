package br.com.ozcorp;

public class Gerente extends Funcionario {
	
	void Gerente() {

		Gerente diana = new Gerente();
		diana.setNome(" Mulher Maravilha");
		diana.setRg("93.185.545-8");
		diana.setCpf("465.824.334-51");
		diana.setMatricula("6549513");
		diana.setEmail("maravilha.mulher@liga.com");
		diana.setSenha("t3m1c3r4");
		diana.setCargo("Gerente de RH");
		diana.setSalario( 6000.00);
		diana.setTs("O+");
		diana.setSexo("Feminino");
		diana.setAcesso(2);

		System.out.println("NOME:               " + diana.getNome());
		System.out.println("RG:                 " + diana.getRg());
		System.out.println("CPF:                " + diana.getCpf());
		System.out.println("MATRICULA:          " + diana.getMatricula());
		System.out.println("EMAIL:              " + diana.getEmail());
		System.out.println("SENHA:              " + diana.getSenha());
		System.out.println("CARGO:              " + diana.getCargo());
		System.out.println("SALARIO:            " + diana.getSalario());
		System.out.println("TIPO SANGUINEO:     " + diana.getTs());
		System.out.println("SEXO:               " + diana.getSexo());
		System.out.println("NIVEL DE ACESSO:    " + diana.getAcesso());
		System.out.println("\n\n\n");
	}
}
