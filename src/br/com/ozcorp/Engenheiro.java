package br.com.ozcorp;

public class Engenheiro extends Funcionario {
	
	void Engenheiro() {

		Engenheiro verde = new Engenheiro();
		verde.setNome("Lanterna Verde");
		verde.setRg("18.435.029-4");
		verde.setCpf("753.357.564-75");
		verde.setMatricula("7942651");
		verde.setEmail("verde.lanterna@liga.com");
		verde.setSenha("grinLanterni");
		verde.setCargo("Cargo: Engenheiro Computacional");
		verde.setSalario(4000.00);
		verde.setTs("B-");
		verde.setSexo("Masculino");
		verde.setAcesso(3);

		System.out.println("NOME:               " + verde.getNome());
		System.out.println("RG:                 " + verde.getRg());
		System.out.println("CPF:                " + verde.getCpf());
		System.out.println("MATRICULA:          " + verde.getMatricula());
		System.out.println("EMAIL:              " + verde.getEmail());
		System.out.println("SENHA:              " + verde.getSenha());
		System.out.println("CARGO:              " + verde.getCargo());
		System.out.println("SALARIO:            " + verde.getSalario());
		System.out.println("TIPO SANGUINEO:     " + verde.getTs());
		System.out.println("SEXO:               " + verde.getSexo());
		System.out.println("NIVEL DE ACESSO:    " + verde.getAcesso());
		System.out.println("\n\n\n");
	}
}
